FROM ubuntu:bionic
ARG DTNOW_DATA=/var/lib/dtnow

RUN set -x \
&& mkdir -p /code $DTNOW_DATA \
;

COPY dtnow /code

ENV DTNOW_DATA $DTNOW_DATA

ENTRYPOINT ["/code/dtnow"]
