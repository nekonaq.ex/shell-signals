UNITDIR		= /lib/systemd/system
BINDIR		= /var/tmp

SERVICES	= dtnow

all:; @echo "No target specified."

#//
$(addsuffix .install,$(SERVICES)) : %.install : $(BINDIR)/%

$(BINDIR)/%: % FORCE
	install -o root -g root -m 755 $< $@

$(addsuffix .service.install,$(SERVICES)) : %.install : $(UNITDIR)/%
	systemctl daemon-reload

$(addsuffix .service.remove,$(SERVICES)) : %.remove : FORCE
	rm -f $(UNITDIR)/$*
	systemctl daemon-reload

$(UNITDIR)/%: % FORCE
	$(MAKE) --no-print-directory $(subst .service,.install,$<)
	install -o root -g root -m 644 $< $@

FORCE:
